trigger NewTrigger on Contact (after insert,after Update,after delete) {
    if(Trigger.isAfter && Trigger.isInsert){
        TriggerHelpingClass.insertAccountAnnualRevenue(Trigger.New,Trigger.Old);
    }
}