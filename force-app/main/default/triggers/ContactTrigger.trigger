trigger ContactTrigger on Contact (After insert,After Delete,After Update) {
    if(Trigger.IsInsert)
      ContactTriggerHandler.afterInsertContact(Trigger.New);
    if(Trigger.IsDelete)
     ContactTriggerHandler.afterDeleteContact(Trigger.Old);
    if(Trigger.IsUpdate)
     ContactTriggerHandler.afterUpdateContact(Trigger.New,Trigger.Old);
}